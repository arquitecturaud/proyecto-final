// Ping a remote server, also uses DHCP and DNS.
// 2011-06-12 <jc@wippler.nl>
//
// License: GPLv2


#include <SPI.h>
#include <MFRC522.h>

#include <EtherCard.h>
#define ETH_SS_PIN  2
#define RF_SS_PIN 3
//COSAS RFID
#define RST_PIN 7
//INTERNET
#define TIMEOUT_MS 7000

MFRC522 rfid(RF_SS_PIN, RST_PIN); // Instance of the class

MFRC522::MIFARE_Key key;

// Init array that will store new NUID
byte nuidPICC[4];
char str[32] = "";

//FIN COSAS RFID
//COSAS ETHERNET
// ethernet interface mac address, must be unique on the LAN
static byte mymac[] = { 0x74,0x2D,0x69,0x2D,0x30,0x30 };

byte Ethernet::buffer[500];
static uint32_t timer;

const char website[] PROGMEM = "192.168.0.200";
int state;


// called when a ping comes in (replies to it are automatic)
static void gotPinged (byte* ptr) {
  ether.printIp(">>> ping from: ", ptr);
}
static void my_callback (byte status, word off, word len) {
  off = off+200;
  Serial.println(">>>");
  // Ethernet::buffer[off+600] = 0;
  Serial.print((const char*) Ethernet::buffer + off);
  state=3;
}
/**
* Helper routine to dump a byte array as hex values to Serial.
*/
void printHex(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {

    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}



/**
* Helper routine to dump a byte array as dec values to Serial.
*/
void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}
void array_to_string(byte array[], unsigned int len, char buffer[])
{
  for (unsigned int i = 0; i < len; i++)
  {
    byte nib1 = (array[i] >> 4) & 0x0F;
    byte nib2 = (array[i] >> 0) & 0x0F;
    buffer[i*2+0] = nib1  < 0xA ? '0' + nib1  : 'A' + nib1  - 0xA;
    buffer[i*2+1] = nib2  < 0xA ? '0' + nib2  : 'A' + nib2  - 0xA;
  }
  buffer[len*2] = '\0';
}

BufferFiller bfill;

static word homePage() {

  bfill = ether.tcpOffset();
  bfill.emit_p(PSTR("<html><meta http-equiv='Refresh' Content='50'>INSERTE UNA TARJETA</html>"));
  return bfill.position();
}

static word redirect(char  *eo) {

  bfill = ether.tcpOffset();
  bfill.emit_p(PSTR("<html><head> <meta http-equiv = \"refresh\" content = \"0; url = http://192.168.0.200/arduino/index.html?cliente=$D\" /></head></html>"),eo);
  return bfill.position();
}
//FIN COSAS ETHERNET
void setup () {


  Serial.begin(9600);

  SPI.begin(); // Init SPI bus
  rfid.PCD_Init(); // Init MFRC522

  for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
  }

  Serial.println(F("This code scan the MIFARE Classsic NUID."));
  Serial.print(F("Using the following key:"));
  printHex(key.keyByte, MFRC522::MF_KEY_SIZE);


  //LO RESPECTIVO AL ETHERNET
  // Change 'SS' to your Slave Select pin, if you arn't using the default pin
  if (ether.begin(sizeof Ethernet::buffer, mymac, ETH_SS_PIN) == 0)
  Serial.println(F("Failed to access Ethernet controller"));
  if (!ether.dhcpSetup())
  Serial.println(F("DHCP failed"));

  ether.printIp("IP:  ", ether.myip);
  ether.printIp("GW:  ", ether.gwip);

  #if 1
  // use DNS to locate the IP address we want to ping
  if (!ether.dnsLookup(PSTR("192.168.0.200")))
  Serial.println("DNS failed");
  #else
  ether.parseIp(ether.hisip, "192.168.0.200");
  #endif
  ether.printIp("SRV: ", ether.hisip);
  //FIN RESPECTIVO ETHERNET

  Serial.println();
  state=1;
  str[0]="";

}

void loop () {
     
     if (ether.packetLoop(ether.packetReceive())   && str[0] != "")  {
        Serial.print(str);
              ether.httpServerReply(redirect(str));
    }

  if(state == 1){
       
       
    if ( ! rfid.PICC_IsNewCardPresent())
    return;

    // Verify if the NUID has been readed
    if ( ! rfid.PICC_ReadCardSerial())
    return;

    Serial.print(F("PICC type: "));
    MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
    Serial.println(rfid.PICC_GetTypeName(piccType));

    // Check is the PICC of Classic MIFARE type
    if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
        Serial.println(F("Your tag is not of type MIFARE Classic."));
        return;
      }



      if (rfid.uid.uidByte[0] != nuidPICC[0] ||
        rfid.uid.uidByte[1] != nuidPICC[1] ||
        rfid.uid.uidByte[2] != nuidPICC[2] ||
        rfid.uid.uidByte[3] != nuidPICC[3] ) {
          Serial.println(F("A new card has been detected."));

          // Store NUID into nuidPICC array
          for (byte i = 0; i < 4; i++) {
            nuidPICC[i] = rfid.uid.uidByte[i];
          }


          array_to_string(nuidPICC, 4, str);

          for (byte i = 0; i < 32; i++) {
            if(str[i]==" "){
              str[i]="";
            }
          }

          Serial.println(F("The NUID tag is:"));
          Serial.print(F("In hex: "));
          printHex(rfid.uid.uidByte, rfid.uid.size);
          Serial.println();
          Serial.print(F("In dec: "));

          printDec(rfid.uid.uidByte, rfid.uid.size);
          Serial.print(str);

          Serial.println();

        }else Serial.println(F("Card read previously."));

        // Halt PICC
        rfid.PICC_HaltA();

        // Stop encryption on PCD
        rfid.PCD_StopCrypto1();

    

      }
}
