<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link href="css/estilos_2.css" rel="stylesheet" type="text/css"/>
    <title></title>
  </head>
  <body>
    <?php
      $bd_usuario = "root";
      $bd_pass = "";
      //Objeto prepara la conexion a la base de datos
      $baseDatos =new PDO('mysql:host=localhost; dbname=BDregistrosUsuario',$bd_usuario, $bd_pass);
      //Consulta a la base de datos
      $sql = "select detalles, valorFactura, ahorroFactura from Factura where ID_Factura = ?";
   ?>

      <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <br>
            <h1>LISTA COMPRAS</h1>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                  <tr>
                    <th>Productos</th>
                  </tr>
                </thead>
              <tbody>
                <?php
                  $num_consulta = $_POST['numeroFactura'];
                  $resultado = $baseDatos->prepare($sql);
                  $resultado->execute(array($num_consulta));
                  $registro = $resultado->fetch(PDO::FETCH_ASSOC);
                    echo "<tr>";
                      echo "<th>" . $registro['detalles'] . "</th>";
                    echo "<tr>";
                    echo "<thead class='thead-dark'>";
                    echo "<tr>";
                      echo "<th> valor total : $" . $registro['valorFactura'] . "</th>";
                    echo "<tr>";
                    echo "</thead>";
                    echo "<tr>";
                      echo "<th class='text-primary'> Ahorro : $" . $registro['ahorroFactura'] . "</th>";
                    echo "<tr>";
                  $resultado->closeCursor();
              ?>
              </table>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <a href="descuentos.php" class= "btn btn-info" role= "button"> Regresar a ver descuentos</a>
        </div>
        <br>
        <br>
        <div class="container">
          <a href="index.php" class= "btn btn-info" role= "button"> Salir </a>
        </div>
  </body>
</html>
