<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link href="css/estilos_2.css" rel="stylesheet" type="text/css"/>
    <title></title>
  </head>
  <body>
    <?php
      $bd_usuario = "root";
      $bd_pass = "";
      //Objeto prepara la conexion a la base de datos
      $baseDatos =new PDO('mysql:host=localhost; dbname=BDregistrosUsuario',$bd_usuario, $bd_pass);
      //Consulta a la base de datos
      $sql = "select ID_Factura, fecha, valorFactura, ahorroFactura from Factura where Cliente_ID_usuario = ?";
   ?>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <br>
            <h1>HISTORICO COMPRAS</h1>
          </div>
        </div>
      </div>
    <form action="ListaCompra.php" method="post">
      <div class="row">
        <div class="col-md-12">
          <div class="text-center">
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                    <tr>
                      <th>FECHA</th>
                      <th>TOTAL COMPRA</th>
                      <th>AHORRO</th>
                      <th>LISTA COMPRA</th>
                    </tr>
                  </thead>
              <tbody>
                    <?php
                      $resultado = $baseDatos->prepare($sql);
                      $resultado->execute(array("1"));
                      while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
                        echo "<tr>";
                          echo "<th>" . $registro['fecha'] . "</th>";
                          echo "<th> $ " . $registro['valorFactura'] . "</th>";
                          echo "<th class='text-primary'> $ " . $registro['ahorroFactura'] . "</th>";
                          $valor = $registro['ID_Factura'];
                          echo "<th>" . "<input type='submit'name='numeroFactura' class='btn btn-primary btn-lg' value='" . $registro['ID_Factura'] .   "''>" . "</th>";
                        echo "<tr>";
                      }
                      $resultado->closeCursor();
                  ?>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <a href="descuentos.php" class= "btn btn-info" role= "button"> Regresar a ver descuentos</a>
        </div>
        <br>
        <br>
        <div class="container">
          <a href="index.php" class= "btn btn-info" role= "button"> Salir </a>
        </div>
      </form>
  </body>
</html>
