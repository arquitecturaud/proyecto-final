<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link href="css/estilos_1.css" rel="stylesheet" type="text/css"/>
    <style media="screen">
      .gris { background-color: #F5FFFA; }
    </style>
    <title></title>
  </head>
  <body>
    <div class="container-fluid">
      <form>
        <div>
        <div class="">
          <br>
  		<?php
  			$bd_usuario = "root";
  			$bd_pass = "";
  			//Objeto prepara la conexion a la base de datos
  			$baseDatos =new PDO('mysql:host=localhost; dbname=BDregistrosUsuario',$bd_usuario, $bd_pass);
  			//Consulta a la base de datos
  			$sql = "select priNombre, priApellido, correo, telefono from Cliente where ID_usuario = ?";
  			$resultado = $baseDatos->prepare($sql);
  			$resultado->execute(array("1"));
  			$registro = $resultado->fetch(PDO::FETCH_ASSOC);

  			echo "Nombre usuario : " . $registro['priNombre'] . " " . $registro['priApellido'] . ".<br>";
  			echo "Correo         : " . $registro['correo'] . ".<br>";
  			echo "Telefono       : " . $registro['telefono'] . ".<br>";
  			$resultado->closeCursor();
  	 	?>
        </div>
       </div>
       </form>
     <?php
		$bd_usuario = "root";
		$bd_pass = "";
		//Objeto prepara la conexion a la base de datos
		$baseDatos =new PDO('mysql:host=localhost; dbname=BDregistrosUsuario',$bd_usuario, $bd_pass);
		//Consulta a la base de datos
		$sql = "select nombreProducto,nombreMarca,precioProducto,descuento,precioFinal,ahorro from Producto,Marca where Categoria_ID_Categoria=? and ID_Marca=Marca_ID_Marca";
	 ?>

  <br>
  <h1>CATEGORÍAS</h1>


	<div class="row">
        <div class="col-md-6">
          <div class="text-center">
            <h3>Aseo</h3>
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                  <th>PRODUCTO</th>
                  <th>MARCA</th>
                  <th>PRECIO</th>
                  <th>DESCUENTO</th>
                  <th>PRECIO FINAL</th>
                  <th>AHORRO</th>
                </tr>
            </thead>
          <tbody>
                <?php
                	$resultado = $baseDatos->prepare($sql);
					$resultado->execute(array("C1"));
					while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
						echo "<tr>";
                  			echo "<th>" . $registro['nombreProducto'] . "</th>";
                  			echo "<th>" . $registro['nombreMarca'] . "</th>";
                  			echo "<th> $ " . $registro['precioProducto'] . "</th>";
                  			echo "<th class='text-danger'> $ -" . $registro['descuento'] . "%</th>";
                  			echo "<th> $ " . $registro['precioFinal'] . "</th>";
                  			echo "<th class='text-success'> $ -".  $registro['ahorro'] ."</th>";
            			echo "</tr>";
					}
	            ?>
              </table>
            </div>
          </div>
        </div>

         <div class="col-md-6">
          <div class="text-center">
            <h3>Lacteos</h3>
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                  <th>PRODUCTO</th>
                  <th>MARCA</th>
                  <th>PRECIO</th>
                  <th>DESCUENTO</th>
                  <th>PRECIO FINAL</th>
                  <th>AHORRO</th>
                </tr>
              </thead>
          <tbody>
                <?php
                	$resultado = $baseDatos->prepare($sql);
					$resultado->execute(array("C2"));
					while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
						echo "<tr>";
                  			echo "<th> $ " . $registro['nombreProducto'] . "</th>";
                  			echo "<th> $ " . $registro['nombreMarca'] . "</th>";
                  			echo "<th> $ " . $registro['precioProducto'] . "</th>";
                  			echo "<th class='text-danger'> $ -" . $registro['descuento'] . "%</th>";
                  			echo "<th> $ " . $registro['precioFinal'] . "</th>";
                  			echo "<th class='text-success'> $ -".  $registro['ahorro'] ."</th>";
            			echo "</tr>";
					}
	            ?>
              </table>
            </div>
          </div>
        </div>

     <div class="row">
        <div class="col-md-6">
          <div class="text-center">
            <h3>Mascotas</h3>
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
              	<tr>
                  <th>PRODUCTO</th>
                  <th>MARCA</th>
                  <th>PRECIO</th>
                  <th>DESCUENTO</th>
                  <th>PRECIO FINAL</th>
                  <th>AHORRO</th>
                </tr>
              </thead>
          <tbody>
                <?php
                	$resultado = $baseDatos->prepare($sql);
					$resultado->execute(array("C3"));
					while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
						echo "<tr>";
                  			echo "<th>" . $registro['nombreProducto'] . "</th>";
                  			echo "<th>" . $registro['nombreMarca'] . "</th>";
                  			echo "<th> $ " . $registro['precioProducto'] . "</th>";
                  			echo "<th class='text-danger'> $ -" . $registro['descuento'] . "%</th>";
                  			echo "<th> $ " . $registro['precioFinal'] . "</th>";
                  			echo "<th class='text-success'> $ -".  $registro['ahorro'] ."</th>";
            			echo "</tr>";
					}
	            ?>
              </table>
            </div>
        </div>
     </div>

     <div class="col-md-6">
          <div class="text-center">
            <h3>Verduras</h3>
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                  <th>PRODUCTO</th>
                  <th>MARCA</th>
                  <th>PRECIO</th>
                  <th>DESCUENTO</th>
                  <th>PRECIO FINAL</th>
                  <th>AHORRO</th>
                </tr>
              </thead>
          <tbody>
                <?php
                	$resultado = $baseDatos->prepare($sql);
					$resultado->execute(array("C4"));
					while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
						echo "<tr>";
                  			echo "<th>" . $registro['nombreProducto'] . "</th>";
                  			echo "<th>" . $registro['nombreMarca'] . "</th>";
                  			echo "<th> $ " . $registro['precioProducto'] . "</th>";
                  			echo "<th class='text-danger'> $ -" . $registro['descuento'] . "%</th>";
                  			echo "<th> $ " . $registro['precioFinal'] . "</th>";
                  			echo "<th class='text-success'> $ -".  $registro['ahorro'] ."</th>";
            			echo "</tr>";
					}
	            ?>
              </table>
            </div>
          </div>
        </div>

      <div class="row">
        <div class="col-md-6">
          <div class="text-center">
            <h3>Bebidas</h3>
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                  <th>PRODUCTO</th>
                  <th>MARCA</th>
                  <th>PRECIO</th>
                  <th>DESCUENTO</th>
                  <th>PRECIO FINAL</th>
                  <th>AHORRO</th>
                </tr>
              </thead>
          <tbody>
                <?php
                	$resultado = $baseDatos->prepare($sql);
					$resultado->execute(array("C5"));
					while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
						echo "<tr>";
                  			echo "<th>" . $registro['nombreProducto'] . "</th>";
                  			echo "<th>" . $registro['nombreMarca'] . "</th>";
                  			echo "<th> $ " . $registro['precioProducto'] . "</th>";
                  			echo "<th class='text-danger'> $ - " . $registro['descuento'] . "%</th>";
                  			echo "<th> $ " . $registro['precioFinal'] . "</th>";
                  			echo "<th class='text-success'> $ -".  $registro['ahorro'] ."</th>";
            			echo "</tr>";
					}
	            ?>
              </table>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div class="text-center">
            <h3>Viveres</h3>
            <div class="container d-flex justify-content-center">
              <table class="table table-striped">
                <thead class="thead-dark">
                <tr>
                  <th>PRODUCTO</th>
                  <th>MARCA</th>
                  <th>PRECIO</th>
                  <th>DESCUENTO</th>
                  <th>PRECIO FINAL</th>
                  <th>AHORRO</th>
                </tr>
              </thead>
          <tbody>
                <?php
                	$resultado = $baseDatos->prepare($sql);
					$resultado->execute(array("C6"));
					while ($registro = $resultado->fetch(PDO::FETCH_ASSOC)) {
						echo "<tr>";
                  			echo "<th>" . $registro['nombreProducto'] . "</th>";
                  			echo "<th>" . $registro['nombreMarca'] . "</th>";
                  			echo "<th> $ " . $registro['precioProducto'] . "</th>";
                  			echo "<th class='text-danger'> $ -" . $registro['descuento'] . "%</th>";
                  			echo "<th> $ " . $registro['precioFinal'] . "</th>";
                  			echo "<th class='text-success'> $ -".  $registro['ahorro'] ."</th>";
            			echo "</tr>";
					}
	            ?>
              </table>
            </div>
          </div>
        </div>
        <?php
        	$resultado->closeCursor();
         ?>
        <div class="container">
          <a href="historialCompras.php" class=" btn btn-info " role= "button"> Compras realizadas anteriormente </a>
        </div>
        <br>
        <br>
        <div class="container">
          <a href="index.php" class= "btn btn-info" role= "button"> Salir </a>
        </div>
  </body>
</html>
